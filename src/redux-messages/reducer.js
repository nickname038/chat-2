import {
  ADD_MESSAGE,
  UPDATE_MESSAGE,
  DELETE_MESSAGE,
  LIKE_MESSAGE,
} from "./actionTypes";

const initialState = [];

export default function messagesReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_MESSAGE: {
      const { id, message } = action.payload;
      const newMessage = { id, ...message, isLiked: false };
      return [...state, newMessage];
    }

    case DELETE_MESSAGE: {
      const { id } = action.payload;
      const filteredMessages = state.filter((message) => message.id !== id);
      return filteredMessages;
    }

    case LIKE_MESSAGE: {
      const { id } = action.payload;
      const likedMessage = state.find(message => message.id === id);
      likedMessage.isLiked = !likedMessage.isLiked;
      return [...state];
    }

    case UPDATE_MESSAGE: {
        const { id, text, editedAt } = action.payload;
        const editedMessageOld = state.find(message => message.id === id);
        const indexOfEditedMessageOld = state.indexOf(editedMessageOld);
        const editedMessageNew = { ...editedMessageOld, text, editedAt };
        state.splice(indexOfEditedMessageOld, 1, editedMessageNew);
        return [...state];
      }

    default:
      return state;
  }
}
