import { SHOW_PRELOADER, HIDE_PRELOADER } from "./actionTypes";

const initialState = true;

export default function preloaderReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_PRELOADER: {
      return true;
    }

    case HIDE_PRELOADER: {
      return false;
    }

    default:
      return state;
  }
}
