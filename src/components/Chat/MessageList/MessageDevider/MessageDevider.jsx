import React from "react";
import "./MessageDevider.css";

const daysList = {
  1: "Monday", 
  2: "Tuesday",
  3: "Wednesday",
  4: "Thursday", 
  5: "Friday", 
  6: "Saturday",
  7: "Sunday"
};

const monthList = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

class MessageDevider extends React.Component {
  render() {
    const date = new Date(this.props.date);
    const day = date.getDay();
    const month = date.getMonth();
    const dateOfMonth = date.getDate();
    return <div className="messages-divider">{`${daysList[day]}, ${dateOfMonth} ${monthList[month]}`}</div>;
  }
}

export default MessageDevider;
