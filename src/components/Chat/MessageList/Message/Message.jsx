import React from "react";
import "./Message.css";
import MessageMain from "./MessageMain/MessageMain";
import MessageDevider from "../MessageDevider/MessageDevider";

class Message extends React.Component {
  constructor(props) {
    super(props);
    this.onLike = this.onLike.bind(this);
  }

  onLike(e) {
    this.props.onLikeMessage(this.props.message.id);
  }
  render() {
    return (
      <div className="message-devider-wrapper">
        {this.props.isNewDayMessage && <MessageDevider date={this.props.date}/>}
        <div className={`message ${this.props.message.isLiked ? " message-liked" : ""}`}>
          <img
            className="message-user-avatar"
            src={this.props.message.avatar}
            alt=""
          />
          <div className="massage-info-wrapper">
            <MessageMain
              text={this.props.message.text}
              time={this.props.message.createdAt}
            />
            <div className="message-footer">
              <button className="message-like" onClick={this.onLike}>
                <span>Like</span>
                <span className="like-icon"> ❤</span>
              </button>
              <span className="message-user-name">
                {this.props.message.user}
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Message;
