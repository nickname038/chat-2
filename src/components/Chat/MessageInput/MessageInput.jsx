import React from "react";
import "./MessageInput.css";

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    const textarea = document.querySelector("textarea");
    const text = textarea.value;
    textarea.value = "";
    this.props.onCreateMessage(text);
  }

  render() {
    return (
      <div className="message-input">
        <form>
          <textarea
            className="message-input-text"
            placeholder="Message"
          ></textarea>
          <button
            type="button"
            className="message-input-button"
            onClick={this.onClick}
          >
           Send
          </button>
        </form>
      </div>
    );
  }
}

export default MessageInput;
