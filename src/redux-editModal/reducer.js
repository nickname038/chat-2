import { SHOW_MODAL, HIDE_MODAL } from "./actionTypes";

const initialState = false;

export default function editModalReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_MODAL: {
      return true;
    }

    case HIDE_MODAL: {
      return false;
    }

    default:
      return state;
  }
}
