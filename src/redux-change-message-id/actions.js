import { SET_CHANGE_MESSAGE_ID } from "./actionTypes";

export const setChangeMessageId = (id) => ({
  type: SET_CHANGE_MESSAGE_ID,
  payload: {
    id,
  },
});
